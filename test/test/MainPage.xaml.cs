﻿namespace test;

public partial class MainPage : ContentPage
{
	int count = 0;

	public MainPage()
	{
		InitializeComponent();
	}

	private void buttonClicked(object sender, EventArgs e)
	{
		Navigation.PushAsync(new SecondPage());
	}
}  